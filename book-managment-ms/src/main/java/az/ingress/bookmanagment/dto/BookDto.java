package az.ingress.bookmanagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class BookDto {


    private Long id ;

    @NotNull
    private  String name;

    private  String publisher;

    @NotNull
    private  String author;

    private Integer price;


}
