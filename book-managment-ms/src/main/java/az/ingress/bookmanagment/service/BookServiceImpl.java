package az.ingress.bookmanagment.service;

import az.ingress.bookmanagment.dto.BookDto;
import az.ingress.bookmanagment.exception.BookNotFoundException;
import az.ingress.bookmanagment.exception.ExistBookException;
import az.ingress.bookmanagment.model.Book;
import az.ingress.bookmanagment.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
@Service

public class BookServiceImpl implements  BookService {

    private final BookRepository bookRepository;
    private final ModelMapper mapper;

    @PersistenceContext
    EntityManager em;



    @Override
    public List<BookDto> getAllBooks() {
        List<Book> books= bookRepository.findAll();
        Type listType = new TypeToken<List<BookDto>>() {
        }.getType();

        return mapper.map(books, listType);
    }

    @Override
    public Page<Book> findAllWithPaginationSort(int page, int size, String field){


        return bookRepository.findAll(PageRequest.of(page, size).withSort(Sort.by(field).ascending()));
    }
    @Override
    public Page<Book> findAllWithPagination(int page, int size){


        return bookRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public BookDto addBook(BookDto dto, Principal principal) {
        Optional<Book>  optional=bookRepository.findByNameAndAuthor(dto.getName(),dto.getAuthor());
        if(optional.isPresent()) throw new ExistBookException("This book is already added");
         dto.setPublisher(principal.getName());
        Book book =mapper.map(dto,Book.class);

        return mapper.map(bookRepository.save(book),BookDto.class);
    }

    @Override
    public void updateBook(BookDto dto ,Long id) {
       bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException("Book not found with this id"));

       dto.setId(id);
       Book book=mapper.map(dto,Book.class);
       bookRepository.save(book);
    }

    public List<BookDto> searchByAllParam(BookDto map) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        Root<Book> bookRoot = cq.from(Book.class);
        List<Predicate> predicates = new ArrayList<>();


        if (map.getName() != null) {
            predicates.add(cb.equal(bookRoot.get("name"), map.getName()));
        }

        if (map.getPrice() != null) {
            predicates.add(cb.equal(bookRoot.get("price"), map.getPrice()));
        }
        if (map.getPublisher()!= null) {
            predicates.add(cb.equal(bookRoot.get("publisher"), map.getPublisher()));
        }

        if (map.getAuthor()!= null) {
            predicates.add(cb.equal(bookRoot.get("author"), map.getAuthor()));
        }



        cq.where(predicates.toArray(new Predicate[0]));
        TypedQuery<Book> query = em.createQuery(cq);
        List<Book> resultList = query.getResultList();
        Type listType = new TypeToken<List<BookDto>>() {
        }.getType();


        return mapper.map(resultList, listType);

    }




}
