package az.ingress.bookmanagment.service;

import az.ingress.bookmanagment.dto.BookDto;
import az.ingress.bookmanagment.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;


public interface BookService {

    List<BookDto> getAllBooks();

    Page<Book> findAllWithPaginationSort(int page, int size, String field);
    Page<Book> findAllWithPagination(int page, int size);

    BookDto addBook(BookDto dto, Principal principal);

    void  updateBook(BookDto dto,Long id);

    List<BookDto> searchByAllParam(BookDto map);
}
