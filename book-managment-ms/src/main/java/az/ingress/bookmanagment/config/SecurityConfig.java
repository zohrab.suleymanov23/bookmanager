package az.ingress.bookmanagment.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
@Configuration
//@EnableGlobalMethodSecurity(
//        prePostEnabled = true, // (1)
//        securedEnabled = true, // (2)
//        jsr250Enabled = true) // (3)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilterConfigurer jwtFilterConfigurer;

    @Override
    protected void configure(HttpSecurity http) throws Exception {  // (2)
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/book**").permitAll()
                .antMatchers("/book/all").hasAnyRole("PUBLISHER","USER")
                .antMatchers("/book/add").hasAnyRole("PUBLISHER")
                .antMatchers("/book/update").hasAnyRole("PUBLISHER")
                .anyRequest().authenticated() // (4)
                .and()
                .httpBasic(); // (7)

       http.apply(jwtFilterConfigurer);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
