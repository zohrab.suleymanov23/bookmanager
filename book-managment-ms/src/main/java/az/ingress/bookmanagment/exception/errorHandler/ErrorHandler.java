package az.ingress.bookmanagment.exception.errorHandler;

import az.ingress.bookmanagment.dto.ExceptionResponse;
import az.ingress.bookmanagment.exception.BookNotFoundException;
import az.ingress.bookmanagment.exception.ExistBookException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ExistBookException.class)
   @ResponseStatus(HttpStatus.CONFLICT)
    public ExceptionResponse handleExistException (ExistBookException ex ){

        log.error("Exception {}", ex);

        return new ExceptionResponse(ex.getMessage());
    }

    @ExceptionHandler(BookNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleBookNotFoundEx(BookNotFoundException ex){

        log.error("Exception {}",ex);
        return new ExceptionResponse(ex.getMessage());
    }


}
