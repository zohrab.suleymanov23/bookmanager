package az.ingress.bookmanagment.exception;

import lombok.Getter;

@Getter
public class BookNotFoundException extends RuntimeException {

    private String message;

    public  BookNotFoundException(String message){
        super(message);
        this.message=message;
    }

}
