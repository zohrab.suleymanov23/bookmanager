package az.ingress.bookmanagment.exception;

import lombok.Getter;

@Getter
public class ExistBookException extends  RuntimeException{

    private String message;

    public ExistBookException(String message){
        super(message);
        this.message=message;
    }


}
