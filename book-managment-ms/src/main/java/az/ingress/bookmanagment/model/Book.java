package az.ingress.bookmanagment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = Book.TABLE_NAME)
public class Book {

    public static final String TABLE_NAME ="books";
    private final   Long serialVersionUID = 4L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column(nullable = false ,unique = true)
    private  String name;

    @Column(nullable = false)
    private String publisher;

    @Column(nullable = false)
    private String author;


    private Integer price;

}
