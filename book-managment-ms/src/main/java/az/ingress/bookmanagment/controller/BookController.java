package az.ingress.bookmanagment.controller;

import az.ingress.bookmanagment.dto.BookDto;
import az.ingress.bookmanagment.model.Book;
import az.ingress.bookmanagment.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/book")
@Slf4j
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping()
    public String enterBookZone(){
        return "Entered Book Zone";
    }


    @GetMapping("/all")
    public List<BookDto> getAllBooks (Principal principal){
        log.info("In BookController included principal {}",principal);
        return bookService.getAllBooks();
    }

    @GetMapping("/pagination/{page}/{size}/{field}")
    public Page<Book> getALlWithPaginationWithSorted(@PathVariable int page ,@PathVariable int size,@PathVariable String field ){

        return bookService.findAllWithPaginationSort(page,size,field);
    }

    @GetMapping("/pagination/{page}/{size}")
    public Page<Book> getALlWithPagination(@PathVariable int page ,@PathVariable int size ){

        return bookService.findAllWithPagination(page,size);
    }

    @GetMapping("/spesific")
    public List<BookDto> getSpecificBooks(@RequestBody BookDto dto) {

     return bookService.searchByAllParam(dto);
    }

    @PostMapping("/add")
    public ResponseEntity<BookDto> addBook(@RequestBody @Valid BookDto dto , Principal principal){
        return ResponseEntity.ok().body(bookService.addBook(dto,principal));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Void> updateBook(@RequestBody  BookDto dto ,@PathVariable @Valid Long id) {
        bookService.updateBook(dto, id);
       return ResponseEntity.ok().build();
    }



}
