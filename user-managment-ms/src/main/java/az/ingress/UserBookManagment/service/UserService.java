package az.ingress.UserBookManagment.service;


import az.ingress.UserBookManagment.dto.SignUpDto;
import az.ingress.UserBookManagment.model.User;

public  interface UserService {

    User createPublisher(SignUpDto signUpDto);
    User addUser(SignUpDto signUpDto);

}
