package az.ingress.UserBookManagment.service.impl;

import az.ingress.UserBookManagment.dto.SignUpDto;
import az.ingress.UserBookManagment.exceptions.UserRegisteredException;
import az.ingress.UserBookManagment.model.Authority;
import az.ingress.UserBookManagment.model.User;
import az.ingress.UserBookManagment.model.enums.RoleName;
import az.ingress.UserBookManagment.repository.AuthorityRepository;
import az.ingress.UserBookManagment.repository.UserRepository;
import az.ingress.UserBookManagment.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private  final ModelMapper mapper;
    private final PasswordEncoder passwordEncoder;


    @Transactional
    @Override
    public User createPublisher(SignUpDto signUpDto) {
        log.info("created publisher is started");
        Optional<User> user = userRepository.findByUsername(signUpDto.getUsername());
        if(user.isPresent()){
            log.info("createPublisher error -- user already registered");
            throw new UserRegisteredException("Publish_User Already Registered");
        }


        Authority authority;
         Optional <Authority> optional=authorityRepository.findByAuthority(RoleName.PUBLISHER.getName());

          if(optional.isPresent()) authority=optional.get();
                  else authority=Authority.builder().authority(RoleName.PUBLISHER.getName()).build();

        User userEntity = mapper.map(signUpDto, User.class);

        userEntity.setAuthorities(Set.of(authority));
        userEntity.setPassword(passwordEncoder.encode(signUpDto.getPassword()));

       return userRepository.save(userEntity);

    }

    @Override
    public User addUser(SignUpDto signUpDto) {

        log.info("created User is started");

        Optional<User> user = userRepository.findByUsername(signUpDto.getUsername());

        if(user.isPresent()){
            log.info("createdUser error -- user already registered");
            throw new UserRegisteredException("User Already Registered");
        }

        Authority authority;
        Optional <Authority> optional=authorityRepository.findByAuthority(RoleName.USER.getName());

        if(optional.isPresent()) authority=optional.get();
        else authority=Authority.builder().authority(RoleName.USER.getName()).build();

        User userEntity = mapper.map(signUpDto, User.class);

        userEntity.setAuthorities(Set.of(authority));
        userEntity.setPassword(passwordEncoder.encode(signUpDto.getPassword()));

     return    userRepository.save(userEntity);
    }
}
