package az.ingress.UserBookManagment.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RoleName {

    PUBLISHER("ROLE_PUBLISHER"),
    USER("ROLE_USER");

    private String name;
}
