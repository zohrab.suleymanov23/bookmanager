package az.ingress.UserBookManagment.controller;

import az.ingress.UserBookManagment.dto.JwtDto;
import az.ingress.UserBookManagment.security.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;

@Slf4j
@RestController
@RequestMapping("/store")
@RequiredArgsConstructor
public class JwtController {

    private final JwtService jwtService;

    @PostMapping("/sign-in")
    public ResponseEntity<JwtDto> signIn(Authentication authentication) {
        log.info("Creating token for authentication {}", authentication);
        if(authentication==null) return  ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

        return ResponseEntity.ok().body(new JwtDto(jwtService.issueToken(authentication,Duration.ofDays(1))));
    }

}
