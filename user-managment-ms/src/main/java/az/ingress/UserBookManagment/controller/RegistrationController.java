package az.ingress.UserBookManagment.controller;

import az.ingress.UserBookManagment.dto.SignUpDto;
import az.ingress.UserBookManagment.model.User;
import az.ingress.UserBookManagment.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/store")
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @PostMapping("/sign-up")
    public ResponseEntity<User> signUp (@RequestBody @Valid SignUpDto signUpDto){

        log.info("in SignUP controller");

        return ResponseEntity.ok().body(userService.createPublisher(signUpDto));
    }


}
