package az.ingress.UserBookManagment.controller;

import az.ingress.UserBookManagment.dto.SignUpDto;
import az.ingress.UserBookManagment.model.User;
import az.ingress.UserBookManagment.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/store")
public class    UserController {

    private final UserService userService;

    @GetMapping
    public  String enterBookStore(){

        return "Welcome the Virtual books  store";
    }

    @PostMapping("/add-user")
    public ResponseEntity<User>  addUser(@RequestBody @Valid SignUpDto signUpDto){

        return  ResponseEntity.ok().body(userService.addUser(signUpDto));
    }

    @GetMapping("/test")
    public  String test(){

        return "TEST";
    }


}
