package az.ingress.UserBookManagment.exceptions;

import lombok.Getter;

@Getter
public class UserRegisteredException extends RuntimeException {
    private String message;

    public UserRegisteredException(String message) {
        super(message);
        this.message = message;
    }
}
