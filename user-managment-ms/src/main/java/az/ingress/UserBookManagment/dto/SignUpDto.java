package az.ingress.UserBookManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SignUpDto {

    @NotNull
    private String username;

    @NotNull
    @Size(min = 4)
    private  String password;

    private Integer age;





}
