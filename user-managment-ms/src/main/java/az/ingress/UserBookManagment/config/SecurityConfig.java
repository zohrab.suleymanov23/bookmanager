package az.ingress.UserBookManagment.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilterConfigurer jwtFilterConfigurer;

    @Override
    protected void configure(HttpSecurity http) throws Exception {  // (2)
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/store**").permitAll()
                .antMatchers("/store/sign-up").permitAll()
                .antMatchers("/store/sign-in").permitAll()
                .antMatchers("/store/add-user").hasAnyRole("PUBLISHER")
                .anyRequest().authenticated() // (4)
                .and()
                .httpBasic(); // (7)

        http.apply(jwtFilterConfigurer);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
