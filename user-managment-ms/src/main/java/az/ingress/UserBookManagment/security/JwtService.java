package az.ingress.UserBookManagment.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JwtService {

    private final String strKey = "ZWp3YmRld2dmIHhhbXBsZSBvZiBqc3d0IGVuY29kaW5nIGtleSB0aGlzIGlzIGFuIGV4YW1wbGUgb2YganN3dCBlbmNvZGluZyBrZXkgdGhpcyBpcyBhbiBleGFtcGxlIG9mIGpzd3QgZW5jb2Rpbmcga2V5IA==";
    private final Key key;

    public JwtService() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(strKey);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String issueToken(Authentication authentication, Duration duration) {
        log.trace("Creating JWT token to {} for {}", authentication, "3600 sec");

        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim("roles", getRoles(authentication));

        return jwtBuilder.compact();
    }

    private Set<String> getRoles(Authentication authentication) {
        return authentication
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


}
